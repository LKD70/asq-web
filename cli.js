'use strict';

const fs = require('fs');
const gamedig = require('gamedig');
const rp = require('request-promise-native');
const term = require('terminal-kit').terminal;

const input = { servers: [] };
const ports = {
	ark: [ 27015, 27017, 27019, 27021, 27023 ],
	atlas: [ 57555, 57557, 57559, 57561 ]
};

const outputServers = () => {
	const output = {};
	input.servers.forEach((server) => output[server.id] = server);
	if (fs.existsSync('public/servers.json')) fs.unlinkSync('public/servers.json');
	try {
		fs.writeFileSync('public/servers.json', JSON.stringify(output, null, 4));
	} catch (e) {
		term.red(`\nAn error occured whilst writing the servers to the public/servers.json file: ${e}`);
		process.exit();
	}
	term.green('\nCompleted. public/servers.json updated!\n');
	process.exit();
};

const atlasScanFinished = () => {
	const server_count = input.servers.length;
	input.servers = input.servers.filter((server, i, s) => s.findIndex(t => t.steam_id === server.steam_id) === i);
	if (input.servers.length < server_count) term.cyan(`\nRemoved ${server_count - input.servers.length} duplicate atlas entries.`);
	outputServers();
};

const scanAtlasServers = () => {
	term.green('\n[ASQ] - Starting to scan for official Atlas servers...\n');
	const progressBar = term.progressBar({
		percent: true,
		title: `Checking Servers:`
	});
	let progress = 0;

	rp({ uri: 'http://atlasdedicated.com/pc/officialservers.ini' }).then(res => {
		const servers = res.split('\n').map(s => s.split('\r')).map(s => s.length ? s : null);
		servers.forEach(server => {
			ports.atlas.map(port => gamedig.query({
				attemptTimeout: 10000,
				host: server,
				maxAttempts: 6,
				port,
				socketTimeout: 5000,
				type: 'protocol-valve'
			}).then(state => {
				const [ , loc, mode, name ] = state.raw.rules.ATLASFRIENDLYNAME_s.match(/\[(\w{2}) (\w{3,10})\] ([a-zA-Z' 0-9]+)/); // eslint-disable-line  no-unused-vars

				const details = {
					battleeye: state.raw.rules.SERVERUSESBATTLEYE_b,
					cluster: 'AT_' + mode,
					game: 'atlas',
					game_port: state.raw.port,
					id: 'AT_',
					ip: server,
					legacy: state.raw.rules.LEGACY_i === '1',
					map: state.map,
					maxplayers: state.maxplayers,
					name: state.name,
					pretty_name: state.name.split(' - (v')[0],
					query_port: port,
					steam_id: parseInt(state.raw.steamid),
					type: mode
				};
				details.id += state.raw.rules.LEGACY_i === '1' ? 'LEG_' : '';
				details.id += state.map === 'Ocean' ? 'Ocean' : 'Unknown';
				const regex = state.raw.rules.CUSTOMSERVERNAME_s.match(/_(\w\d+)/);
				details.id += '_';
				details.id += regex ? regex[1] : state.raw.rules.CUSTOMSERVERNAME_s.split(' ').map(s => s.charAt(0).toUpperCase() + s.substring(1)).join('');
				progress += 1;
				progressBar.update(progress * 100 / servers.length / 500);
				if (server === servers[servers.length - 1] && port === ports.atlas[ports.atlas.length - 1]) atlasScanFinished();
				input.servers.push(details);
			}).catch(() => {
				if (server === servers[servers.length - 1] && port === ports.atlas[ports.atlas.length - 1]) atlasScanFinished();
				progress += 1;
				progressBar.update(progress * 100 / servers.length / 500);
			}));
		});
	});
};

const arkScanFinished = () => {
	const server_count = input.servers.length;
	input.servers = input.servers.filter((server, i, s) => s.findIndex(t => t.steam_id === server.steam_id) === i);
	if (input.servers.length < server_count) term.cyan(`\nRemoved ${server_count - input.servers.length} duplicate ark entries.`);
	term.green('\nWould you like to scan for Atlas servers too? [Y|n]\n');
	term.yesOrNo({ no: [ 'n' ], yes: [ 'y', 'ENTER' ] }, (err, res) => {
		if (err) {
			console.log(err);
			process.exit();
		}
		if (res) {
			scanAtlasServers();
		} else {
			outputServers();
		}
	});
};

const scanArkServers = () => {
	term.green('[ASQ] - Starting to scan for official Ark servers...\n');
	const progressBar = term.progressBar({
		percent: true,
		title: `Checking Servers:`
	});
	let progress = 0;

	rp({ uri: 'http://arkdedicated.com/officialservers.ini' }).then(res => {
		const servers = res.split('\n').map(line => {
			const elements = line.split(' //');
			if (elements.length > 1 && typeof elements !== 'undefined') return elements;
			return null;
		}).filter(s => s !== null);
		servers.forEach(server => {
			ports.ark.map(port => gamedig.query({
				attemptTimeout: 10000,
				host: server[0],
				maxAttempts: 6,
				port,
				socketTimeout: 5000,
				type: 'protocol-valve'
			}).then(state => {
				const details = {
					battleeye: state.raw.rules.SERVERUSESBATTLEYE_b === 'true',
					cluster: state.raw.rules.ClusterId_s,
					game: 'ark',
					game_port: state.raw.port,
					id: '',
					ip: server[0],
					legacy: state.raw.rules.LEGACY_i === '1',
					map: state.map,
					maxplayers: state.maxplayers,
					name: state.name,
					pretty_name: state.name.split(' ')[0],
					query_port: port,
					steam_id: parseInt(state.raw.steamid),
					type: state.raw.rules.ClusterId_s === 'PCPVE' ? 'PvE'
						: state.raw.rules.ClusterId_s === 'NewPCPVE' ? 'PvE'
							: state.raw.rules.ClusterId_s === 'PCPVP' ? 'PvP'
								: state.raw.rules.ClusterId_s === 'NewPCPVP' ? 'PvP'
									: state.raw.rules.ClusterId_s === 'PCSmallTribes' ? 'SmallTribes'
										: state.raw.rules.ClusterId_s === 'PCArkpocalypse' ? 'Arkpocalypse'
											: state.raw.rules.ClusterId_s === 'NewPCHardcorePVP' ? 'Hardcore'
												: state.raw.rules.ClusterId_s === 'PCPrimPVE' ? 'Primitive'
													: state.raw.rules.ClusterId_s
				};
				details.id = state.raw.rules.LEGACY_i === '1' ? 'LEG_' : '';
				details.id += state.raw.rules.ClusterId_s === 'PCSmallTribes' ? 'ST_' : '';
				details.id += state.raw.rules.ClusterId_s === 'PCArkpocalypse' ? 'POC_' : '';
				details.id += state.raw.rules.ClusterId_s === 'NewPCHardcorePVP' ? 'HC_' : '';
				details.id += state.raw.rules.ClusterId_s === 'PCPrimPVE' ? 'PRIM_' : '';
				details.id += (state.map === 'ScorchedEarth' ? 'SE'
					: state.map === 'Genesis' ? 'Gen'
						: state.map === 'TheIsland' ? 'Island'
							: state.map === 'Ragnarok' ? 'Rag'
								: state.map === 'TheCenter' ? 'Center'
									: state.map === 'Aberration' ? 'Ab'
										: state.map === 'Extinction' ? 'Ext'
											: state.map === 'Valguero' ? 'Valg'
												: state.map === 'CrystalIsles' ? 'Cry'
													: 'Unknown') + state.raw.rules.CUSTOMSERVERNAME_s
					.match(/(\d+)(?!.*\d)[abc]{0,1}/)[0];
				while (input.servers.some(s => s.id === details.id)) {
					details.id += '_';
				}

				progress += 1;
				progressBar.update(progress * 100 / servers.length / 500);
				if (server === servers[servers.length - 1] && port === ports.ark[ports.ark.length - 1]) arkScanFinished();
				input.servers.push(details);
			}).catch(() => {
				if (server === servers[servers.length - 1] && port === ports.ark[ports.ark.length - 1]) arkScanFinished();
				progress += 1;
				progressBar.update(progress * 100 / servers.length / 500);
			}));
		});
	});
};

scanArkServers();
