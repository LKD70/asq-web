'use strict';

const express = require('express');
const router = express.Router();
const servers = require('../public/servers.json');
const gamedig = require('gamedig');

const formatSeconds = (sec, and = true) => {
	sec = Number(sec);
	const d = Math.floor(sec / 86400);
	const h = Math.floor(sec % 86400 / 3600);
	const m = Math.floor(sec % 86400 % 3600 / 60);
	const s = Math.floor(sec % 86400 % 3600 % 60);

	return (d > 0 ? h + (h === 1 ? ' day' : ' days') : '') +
			(h > 0 ? (d ? ', ' : '') + h + (h === 1 ? ' hour' : ' hours') : '') +
			(m > 0 ? (h ? ', ' : '') + m + (m === 1 ? ' minute' : ' minutes') : '') +
			(s > 0 ? (m ? ', ' + (and ? 'and ' : '') : '') + s + (s === 1 ? ' second' : ' seconds') : '');
};

router.get('/:id?/:auto?/:time?', (req, res) => {
	if (req.params.id in servers) {
		gamedig.query(
			{
				host: servers[req.params.id].ip,
				port: servers[req.params.id].query_port,
				type: 'protocol-valve'
			},
			(e, state) => {
				if (state === null) {
					if (req.query.json) {
						res.status(408).json({
							error: true,
							message: `The server didn't respond. Perhaps it's offline?`
						});
					} else {
						res.status(408).render('customerror', {
							docs: 'server',
							error: `No Server`,
							title: `The server didn't respond. Perhaps it's offline?`
						});
					}
				} else {
					const players = [];
					for (let s = 0; s < state.players.length; s++) {
						const player = state.players[s];
						if (player.name != null) {
							player.time = formatSeconds(Math.floor(player.time));
							players.push(player);
						}
					}

					if (req.query.json) {
						const output = {
							additional: state,
							count: players.length,
							players: players.map(p => ({ name: p.name, time: p.time }))
						};
						res.json(output);
					} else {
						res.render('server', {
							additional: state,
							auto: req.params.auto,
							playercount: players.length,
							players,
							time: parseInt(req.params.time) ? parseInt(req.params.time) < 10 ? 10 : parseInt(req.params.time) : 10,
							title: servers[req.params.id].pretty_name
						});
					}
				}
			}
		);
	} else if (req.query.json) {
		res.status(404).json({ error: true, message: `Server ID wasn't founnd in the config.` });
	} else {
		res.status(404).render('customerror', {
			docs: 'server',
			error: `Server ID not found in the config.`,
			title: `Wrong Server ID`
		});
	}
});

module.exports = router;
