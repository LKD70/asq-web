'use strict';

const express = require('express');
const router = express.Router();
const servers = require('../public/servers.json');

router.get('/', (req, res) => {
	if (Object.keys(servers).length) {
		if (req.query.json) {
			if (req.query.array) {
				const a = Object.keys(servers).map(s => servers[s]);
				res.json(a);
			} else {
				res.json(servers);
			}
		} else {
			res.render('list', {
				servers,
				title: 'Servers List'
			});
		}
	} else if (req.query.json) {
		res.status(503).json({
			error: true,
			message: `Couldn't find server information.`
		});
	} else {
		res.status(503).render('customerror', {
			error: `It seems the server config is empty... This is a site side issue, sorry!`,
			title: `No Servers found`
		});
	}

});

module.exports = router;
